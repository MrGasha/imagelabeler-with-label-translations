import 'dart:io';
//packages
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:translator/translator.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  File pic;
  String selectedLang = 'es';
  Map<String, String> idiomas = {
    'en': 'English',
    'es': 'Español',
    'ru': 'русский',
    'zh-cn': '中文',
    'ja': '日本語',
  };
  List originalLangLabels = [];
  var inputImage;
  String originalText = '';
  var textoDePrueba;
  final imageLabeler = GoogleMlKit.vision.imageLabeler();
  Future<void> galleryPicHandler() async {
    final picker = ImagePicker();
    final imageFile = await picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 600,
    );
    setState(() {
      pic = File(imageFile.path);
      inputImage = InputImage.fromFile(pic);
      originalLangLabels=[];
    });
  }

  Future<void> cammeraPicHanlder() async {
    final picker = ImagePicker();
    final imageFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      pic = File(imageFile.path);
      inputImage = InputImage.fromFile(pic);
      originalLangLabels=[];
    });
  }

  Future<void> onProcesar() async {
    final labels = await imageLabeler.processImage(inputImage);
    final translator = GoogleTranslator();
    List _originalLangVals;
    String _textoDePrueba="";
    labels.forEach((element) {
      _textoDePrueba+=element.label+". ";
     });
     textoDePrueba = await translator.translate(_textoDePrueba, from: 'en',to: selectedLang) ;
     print(textoDePrueba.text);
    _originalLangVals = textoDePrueba.text.toString().split(". ");
    setState(() {
      originalLangLabels=_originalLangVals;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[900],
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'Reconocimiento de Objetos',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width * 0.07,
                          fontWeight: FontWeight.w300,
                          letterSpacing: 0.03,
                          wordSpacing: 2,
                          height: 1.25),
                    ),
                    IconButton(
                      iconSize: 30,
                      color: Colors.white,
                      splashRadius: 22,
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.012),
                      alignment: Alignment.center,
                      icon: Icon(Icons.more_vert_outlined),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (_) {
                            return Dialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              insetPadding: EdgeInsets.symmetric(
                                  vertical:
                                      MediaQuery.of(context).size.height * 0.32,
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.1),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(20),
                                    child: Text(
                                      "Seleccione un idioma",
                                      style: TextStyle(
                                          fontSize: 25, letterSpacing: 0.5),
                                    ),
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                      itemCount: idiomas.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return InkWell(
                                          onTap: () {
                                            selectedLang =
                                                idiomas.keys.elementAt(index);
                                            Navigator.of(context).pop();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: BorderDirectional(
                                              bottom: BorderSide(
                                                color: Colors.black26,
                                              ),
                                            )),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            alignment: Alignment.center,
                                            child: Text(
                                              idiomas.values.elementAt(index),
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  height: 1.1,
                                                  letterSpacing: 0.8),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: pic == null
                          ? Icon(Icons.camera_alt_outlined)
                          : Icon(Icons.camera_alt_rounded),
                      onPressed: () {
                        galleryPicHandler();
                      },
                      color: Colors.white,
                      splashRadius: 22,
                    ),
                    IconButton(
                      icon: pic == null
                          ? Icon(Icons.camera_outlined)
                          : Icon(Icons.camera),
                      onPressed: () {
                        cammeraPicHanlder();
                      },
                      color: Colors.white,
                      splashRadius: 22,
                    ),
                  ],
                ),
                pic == null
                    ? Container(
                        child: Text(
                          "Aun no se ha agregado foto",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.028,
                              letterSpacing: 1.1,
                              fontWeight: FontWeight.w300),
                        ),
                      )
                    : Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        elevation: 5,
                        margin: EdgeInsets.all(18),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.file(pic),
                        ),
                      ),
                pic == null
                    ? SizedBox()
                    : OutlinedButton(
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all(Colors.white),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                          side: MaterialStateProperty.all(
                            BorderSide(color: Colors.white),
                          ),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          ),
                        ),
                        onPressed: () {
                          onProcesar();
                        },
                        child: Text(
                          'Procesar',
                          style: TextStyle(
                            fontSize: 18,
                            letterSpacing: 0.8,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                originalLangLabels.isEmpty
                    ? SizedBox()
                    : Container(
                        margin: EdgeInsets.all(15),
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: originalLangLabels.map(
                              (element) {
                                return Text(
                                  element.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w300,
                                      height: 1.8,
                                      letterSpacing: 1.2),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
